// bài 1: tính tổng các số dương trong array
function tongSoDuong(ar) {
    var tong = 0;
    for (var i = 0; i <= ar.length - 1; i++) {
        if (ar[i] > 0) {
            tong += ar[i];
        }
    }
    return tong;
}

// bài 2: đếm số dương trong array
function demSoDuong(ar) {
    var dem = 0;
    for (var i = 0; i <= ar.length - 1; i++) {
        if (ar[i] > 0) {
            dem++;
        }
    }
    return dem;
}

// bài 3: tìm số nhỏ nhất trong array
function timSoNhoNhat(ar) {
    var min = ar[0];
    for (var i = 0; i <= ar.length - 1; i++) {
        if (ar[i] < min) {
            min = ar[i];
        }
    }
    return min;
}

// bài 4: tìm số DƯƠNG nhỏ nhất trong array
function timSoDuongNhoNhat(ar) {
    var min = ar[0];
    for (var i = 0; i <= ar.length - 1; i++) {
        if (ar[i] < min && ar[i] > 0) {
            min = ar[i];
        }
    }
    if (min < 0) {
        return 'Array không có số dương'
    }
    return min;
}

// bài 5: tìm số chẵn cuối cùng
function timSoChanCuoiCung(ar) {
    var ketQua = -1;
    for (var i = ar.length - 1; i >= 0; i--) {
        if (ar[i] % 2 === 0) {
            ketQua = ar[i];
            break;
        }
    }
    return ketQua;
}

// bài 6: đổi chỗ 2 vị trí trong array
function doiCho(ar, v1, v2) {
    var tam = ar[v1 - 1];
    ar[v1 - 1] = ar[v2 - 1];
    ar[v2 - 1] = tam;
}

// bài 7: sắp xếp mảng tăng dần
function sapXepTang(ar) {
    for (var i = 0; i <= ar.length - 2; i++) {
        for (var k = i + 1; k <= ar.length - 1; k++) {
            if (ar[k] < ar[i]) {
                doiCho(ar, i + 1, k + 1);
            }
        }
    }
}

// bài 8: tìm số nguyên tố đầu tiên
function timSoNguyenToDauTien(ar) {
    var ketQua = -1;
    for (var i = 0; i <= ar.length - 1; i++) {
        if (ar[i] > 1
            && (ar[i] === 2
                || ar[i] === 3
                || ar[i] === 5
                || ar[i] === 7
                || (ar[i] % 2 !== 0
                    && ar[i] % 3 !== 0
                    && ar[i] % 5 !== 0
                    && ar[i] % 7 !== 0))
        ) {
            ketQua = ar[i];
            break;
        }
    }
    return ketQua;
}

// bài 9: đếm số nguyên trong mảng
function demSoNguyen(ar) {
    var dem = 0;
    for (var i = 0; i <= ar.length - 1; i++) {
        // số nguyên là số khi mà nhân 10 lên rồi chia lấy dư cho 10 ra 0
        if (ar[i] * 10 % 10 === 0) {
            dem++;
        }
    }
    return dem;
}

// bài 10: so sánh số lượng âm dương trong mảng
function soSanhAmDuong(ar) {
    var soLuongDuong = 0;
    var soLuongAm = 0;
    for (var i = 0; i <= ar.length - 1; i++) {
        if (ar[i] > 0) {
            soLuongDuong++;
        }
        if (ar[i] < 0) {
            soLuongAm++;
        }
    }
    if (soLuongDuong > soLuongAm) {
        return 'Dương nhiều hơn'
    }
    if (soLuongDuong < soLuongAm) {
        return 'Âm nhiều hơn'
    }
    if (soLuongDuong === soLuongAm) {
        return 'Bằng nhau'
    }
}

/////////////////////////////////////////////////////////////////////////
var array1 = [];
// var array1 = [5, -1, 7, 5, -7, 1, 0, -5, -6, 6, 9, 2, 0, -9, 4, 3];
function pushItem() {
    var item = document.getElementById('addItem').value * 1;
    array1.push(item);

    document.getElementById('m1').innerText = array1.toString();
    document.getElementById('addItem').value = '';
}
function clearArr() {
    array1 = [];
    document.getElementById('m1').innerText = array1.toString();
}

function bai1() {
    document.getElementById('bai1').innerText = tongSoDuong(array1)
}
function bai2() {
    document.getElementById('bai2').innerText = demSoDuong(array1)
}
function bai3() {
    document.getElementById('bai3').innerText = timSoNhoNhat(array1)
}
function bai4() {
    document.getElementById('bai4').innerText = timSoDuongNhoNhat(array1)
}
function bai5() {
    document.getElementById('bai5').innerText = timSoChanCuoiCung(array1)
}
function bai6() {
    var p1 = document.getElementById('viTri1').value * 1;
    var p2 = document.getElementById('viTri2').value * 1;
    doiCho(array1, p1, p2);
    document.getElementById('bai6').innerText = array1.toString()
}
function bai7() {
    sapXepTang(array1);
    document.getElementById('bai7').innerText = array1.toString()
}
function bai8() {
    document.getElementById('bai8').innerText = timSoNguyenToDauTien(array1);
}
function bai9() {
    document.getElementById('bai9').innerText = demSoNguyen(array1);
}
function bai10() {
    document.getElementById('bai10').innerText = soSanhAmDuong(array1);
}